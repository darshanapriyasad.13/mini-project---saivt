
from scipy.misc import imread
from scipy.misc import imresize
import numpy as np

class Preprocess_Images:
    
    def preprocess_images_alexnet(self,image_paths,image_size=(227,227)):
        image_list= []

        for image_path in image_paths:
            # print image_path
            image = imread(image_path, mode = 'RGB')
            
            if image_size:
                image = imresize(image,image_size)

                # image = np.pad(image,163,'constant')
            
            image = image.astype('float32')

            # image[:,:,0] -= 123.68
            # image[:,:,1] -= 116.779
            # image[:,:,2] -= 103.939

            image = image.transpose((2,0,1))
            image_list.append(image)

        image_batch = None

        try:
            image_batch = np.stack(image_list,axis=0)
        except:
            raise ValueError

        return image_batch
