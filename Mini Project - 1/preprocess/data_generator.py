import numpy as np
import random
import sys
sys.path.insert(0, "./")
from preprocess.preprocess_input_images import Preprocess_Images

class FitGenerator:

   
    def generator(self,features, labels, batch_size):
        preprocess = Preprocess_Images()
        batch_features = np.zeros((batch_size, 3, 227, 227))
        batch_labels = np.zeros((batch_size,6))

        while True:
            filew = open('./summary/trained_images.txt','a+')
            filewc = open('./summary/trained_classes.txt','a+')
            for i in range(batch_size):
                # choose random index in features
                index= random.randrange(len(features))
                feature_list = []
                feature_list.append(features[index])
                filew.write(features[index]+'\n')
                batch_features[i] = preprocess.preprocess_images_alexnet(feature_list)
                batch_labels[i] = labels[index]
                filewc.write(features[index]+'\t'+str(labels[index])+'\n')
            yield batch_features, batch_labels
