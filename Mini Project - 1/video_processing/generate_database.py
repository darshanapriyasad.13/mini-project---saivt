import cv2
import sys
sys.path.insert(0, "./")

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import json

# save_as_image(self,image_stack,topic,audio_file,save_path,segment)
# save_dcnn_training_data('/home/darshana/enterface database')

def save_vgg_data(path):
    save_path = None
    with open('./configurations/vgg_configurations.json') as json_data_file:
            data = json.load(json_data_file)
            save_path = data['vgg_frame_extraction']['image_save_path']
    
    train_img=[]
    
    files_folders = os.listdir(path)
    folders = []
    for item in files_folders:
        if os.path.isdir(path+'/'+item):
            folders.append(path+'/'+item)
    
    sub_folders = ['anger','disgust','fear','happiness','sadness','surprise']

    
    # file_dict = {}
    # file_dict['anger']=[]
    # file_dict['disgust']=[]
    # file_dict['fear']=[]
    # file_dict['happiness']=[]
    # file_dict['sadness']=[]
    # file_dict['surprise']=[]

    for folder in folders:
        sub_folders = os.listdir(folder)
        for sub_folder in sub_folders:
            sentences = os.listdir(folder+'/'+sub_folder)
            # print files
            
            for sentence in sentences:
                # print 'executed'
                video_file = [f for f in os.listdir(folder+'/'+sub_folder+'/'+sentence) if f.endswith('.avi')][0]
                # print folder+'/'+sub_folder+'/'+sentence+'/'+video_file
                # print '=='
                vidcap = cv2.VideoCapture(folder+'/'+sub_folder+'/'+sentence+'/'+video_file)

                
                success, image = vidcap.read()
                count =0
                success = True
                image_array = []
                while success:
                    image_array.append(image)
                    # cv2.imwrite('frame%d.jpg'%count,image)
                    success, image = vidcap.read()
                    # print 'Read new frame : ', success

                    count += 1
                # segmented_inputs = mfcc_extraction.generate_dcnn_input(folder+'/'+sub_folder+'/'+sentence+'/'+video_file)
                # segmented_inputs = mfcc_extraction.generate_dcnn_input_as_segments(folder+'/'+sub_folder+'/'+sentence+'/'+video_file)
                for i in range(len(image_array)):
                # for i in range((len(segmented_inputs)-3)/3):
                    # file_dict[sub_folder].append(segmented_inputs[i])
                    # mfcc_extraction.save_as_image(segmented_inputs[i],sub_folder,video_file[:-4],save_path,i)
                    image_path = save_path+'/'+sub_folder+'/'+video_file.split('.')[0]+"_"+str(i)+".jpg"
                    # print image_path
                    cv2.imwrite(image_path,image_array[i])
                    # mfcc_extraction.save_as_image_segments(segmented_inputs[i*3],sub_folder,video_file[:-4],save_path,i,"static")
                    # mfcc_extraction.save_as_image_segments(segmented_inputs[i*3+1],sub_folder,video_file[:-4],save_path,i,"delta")
                    # mfcc_extraction.save_as_image_segments(segmented_inputs[i*3+2],sub_folder,video_file[:-4],save_path,i,"delta_delta")
                # print video_file, sub_folder

save_vgg_data('/home/darshana/enterface database')