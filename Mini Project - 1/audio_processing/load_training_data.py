import sys
sys.path.insert(0, "./")

from scipy.misc import imread
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
from audio_processing.mfcc_feature_extraction import MFCC_Extraction
import json

def save_dcnn_training_data(path):

    save_path = None
    with open('./configurations/mfcc_configurations.json') as json_data_file:
            data = json.load(json_data_file)
            save_path = data['mfcc_test']['image_save_path']
            
    mfcc_extraction = MFCC_Extraction()
    
    train_img=[]
    
    files_folders = os.listdir(path)
    folders = []
    for item in files_folders:
        if os.path.isdir(path+'/'+item):
            folders.append(path+'/'+item)
    
    sub_folders = ['anger','disgust','fear','happiness','sadness','surprise']
    print len(folders)

    file_dict = {}
    file_dict['anger']=[]
    file_dict['disgust']=[]
    file_dict['fear']=[]
    file_dict['happiness']=[]
    file_dict['sadness']=[]
    file_dict['surprise']=[]

    for folder in folders:
        sub_folders = os.listdir(folder)
        print folder
        for sub_folder in sub_folders:
            sentences = os.listdir(folder+'/'+sub_folder)
            # print files
            for sentence in sentences:

                video_file = [f for f in os.listdir(folder+'/'+sub_folder+'/'+sentence) if f.endswith('.avi')][0]
                segmented_inputs = mfcc_extraction.generate_dcnn_input(folder+'/'+sub_folder+'/'+sentence+'/'+video_file)
                # segmented_inputs = mfcc_extraction.generate_dcnn_input_as_segments(folder+'/'+sub_folder+'/'+sentence+'/'+video_file)
                for i in range(len(segmented_inputs)-1):
                # for i in range((len(segmented_inputs)-3)/3):
                    # file_dict[sub_folder].append(segmented_inputs[i])
                    mfcc_extraction.save_as_image(segmented_inputs[i],sub_folder,video_file[:-4],save_path,i)
                    # mfcc_extraction.save_as_image_segments(segmented_inputs[i*3],sub_folder,video_file[:-4],save_path,i,"static")
                    # mfcc_extraction.save_as_image_segments(segmented_inputs[i*3+1],sub_folder,video_file[:-4],save_path,i,"delta")
                    # mfcc_extraction.save_as_image_segments(segmented_inputs[i*3+2],sub_folder,video_file[:-4],save_path,i,"delta_delta")
                # print video_file, sub_folder


# save_as_image(self,image_stack,topic,audio_file,save_path,segment)
save_dcnn_training_data('/home/darshana/enterface database')