import librosa
import librosa.display
import numpy as np,wave
import scipy as sp
import matplotlib.pyplot as plt
from scipy.misc import imresize

class MFCC_Extraction:

    def save_as_image(self,image_stack,topic,audio_file,save_path,segment):
        # print audio_file
        pic_path = save_path+'/'+topic+'/'+audio_file+"_"+str(segment)+".jpg"
        # image_stack = imresize(image_stack,(227*227))
        sp.misc.imsave(pic_path,image_stack)

    def save_as_image_segments(self,image,topic,audio_file,save_path,segment,channel):
        # print audio_file
        pic_path = save_path+'/'+topic+'/'+audio_file+"_"+str(segment)+"_"+channel+".jpg"
        # image_stack = imresize(image_stack,(227*227))
        sp.misc.imsave(pic_path,image)

    def utterence_mel_spectrogram(self,audio_file):
        audio_time_series, sampling_rate = librosa.load(audio_file)
        stft_matrix = librosa.core.stft(audio_time_series, n_fft=25*sampling_rate/1000, hop_length=(10)*sampling_rate/1000, window='hamming', center=True, pad_mode='reflect')
        mel_scaled_spectrogram = librosa.feature.melspectrogram(S=np.abs(stft_matrix),n_mels=64, fmin=20, fmax=8000)
        # print mel_scaled_spectrogram.shape
        return mel_scaled_spectrogram

    def segmented_mel_spectrogram(self,mel_scaled_spectrogram,window_size,overlap_size):
        
        window_step = window_size - overlap_size
        padding = np.zeros((64,(window_step - (mel_scaled_spectrogram.shape[-1]-overlap_size) % window_step)))

        padded_mel_scaled_spectrogram = np.hstack((mel_scaled_spectrogram,padding))

        new_shape = ((padded_mel_scaled_spectrogram.shape[-1] - overlap_size) // window_step,window_size,padded_mel_scaled_spectrogram.shape[0])
        new_strides = (window_step*8,padded_mel_scaled_spectrogram.strides[0],padded_mel_scaled_spectrogram.strides[-1])
        padded_mel_scaled_spectrogram_strided = np.lib.stride_tricks.as_strided(padded_mel_scaled_spectrogram, shape=new_shape, strides=new_strides)

        return padded_mel_scaled_spectrogram_strided

    def normalize(self,spectrum):
        return ((spectrum-np.min(spectrum))/(np.max(spectrum)-np.min(spectrum)))

    def plot_dcnn_inputs(self,static,delta,delta_delta):
        plt.subplot(3, 1, 1)
        librosa.display.specshow(static)
        plt.title('MFCC-static')
        plt.colorbar()
        plt.subplot(3, 1, 2)
        librosa.display.specshow(delta)
        plt.title(r'MFCC-$\Delta$')
        plt.colorbar()
        plt.subplot(3, 1, 3)
        librosa.display.specshow(delta_delta, x_axis='time')
        plt.title(r'MFCC-$\Delta^2$')
        plt.colorbar()
        plt.tight_layout()
        plt.show()
        # plt.savefig('channels.jpg',bbox_inches='tight')

    def generate_dcnn_input(self,audio_file):
        mel_scaled_spectrogram = self.utterence_mel_spectrogram(audio_file)
        mel_scaled_spectrogram_segments = self.segmented_mel_spectrogram(mel_scaled_spectrogram,64,64-30)

        segmented_inputs = []

        for segment in mel_scaled_spectrogram_segments:
            static = self.normalize(librosa.power_to_db(segment, ref=np.max))*255
            delta = self.normalize(librosa.feature.delta(static,order=1))*255
            delta_delta = self.normalize(librosa.feature.delta(static,order=2))*255
            dcnn_input = np.dstack((static,delta,delta_delta))
            segmented_inputs.append(dcnn_input)
        
        return segmented_inputs
    
    def generate_dcnn_input_as_segments(self,audio_file):
        mel_scaled_spectrogram = self.utterence_mel_spectrogram(audio_file)
        mel_scaled_spectrogram_segments = self.segmented_mel_spectrogram(mel_scaled_spectrogram,64,64-30)

        segmented_inputs = []

        for segment in mel_scaled_spectrogram_segments:
            static = self.normalize(librosa.power_to_db(segment, ref=np.max))*255
            delta = self.normalize(librosa.feature.delta(static,order=1))*255
            delta_delta = self.normalize(librosa.feature.delta(static,order=2))*255
            # dcnn_input = np.dstack((static,delta,delta_delta))
            segmented_inputs.append(static)
            segmented_inputs.append(delta)
            segmented_inputs.append(delta_delta)
        
        return segmented_inputs