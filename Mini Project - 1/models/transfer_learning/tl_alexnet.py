import sys
sys.path.insert(0, "./")
from preprocess.data_generator import FitGenerator
from preprocess.preprocess_input_images import Preprocess_Images
from models.modified_alexnet import AlexNet
from keras.layers import Input, Dense, Convolution2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D, Dropout, Flatten, merge, Reshape, Activation
from keras.optimizers import SGD
from sklearn.model_selection import train_test_split
from keras.utils.np_utils import to_categorical
from keras.models import Model
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import log_loss
import json
import numpy as np
from keras import models
import matplotlib.pyplot as plt
import scipy as sp
from shutil import copyfile
from keras.preprocessing.image import ImageDataGenerator

class TransferLearnedFrozenAlexNet:
    
    def transer_to_image_data_generator(X_train,X_valid):

        for i in range(len(X_train)):
            file = X_train[i]
            # print file
            save_file = file.split('/')[-1]
            if '_an_' in file:
                copyfile(file,'./Data/Train/anger/'+save_file)
            if '_fe_' in file:
                copyfile(file,'./Data/Train/disgust/'+save_file)
            if '_di_' in file:
                copyfile(file,'./Data/Train/fear/'+save_file)
            if '_sa_' in file:
                copyfile(file,'./Data/Train/sadness/'+save_file)
            if '_su_' in file:
                copyfile(file,'./Data/Train/surprise/'+save_file)
            if '_ha_' in file:
                copyfile(file,'./Data/Train/happiness/'+save_file)
            
        for i in range(len(X_valid)):
            file = X_valid[i]
            save_file = file.split('/')[-1]
            if '_an_' in file:
                copyfile(file,'./Data/Valid/anger/'+save_file)
            if '_fe_' in file:
                copyfile(file,'./Data/Valid/disgust/'+save_file)
            if '_di_' in file:
                copyfile(file,'./Data/Valid/fear/'+save_file)
            if '_sa_' in file:
                copyfile(file,'./Data/Valid/sadness/'+save_file)
            if '_su_' in file:
                copyfile(file,'./Data/Valid/surprise/'+save_file)
            if '_ha_' in file:
                copyfile(file,'./Data/Valid/happiness/'+save_file)


    def get_tl_frozen_alexnet(self,num_classes):
        alexnet = AlexNet().get_alexnet_model()
        with open('./configurations/tl_configurations.json') as json_data_file:
            data = json.load(json_data_file)
            weight_file = data['tl_data']['weights_path']
        # image_directory = data['tl_data']['images_dir_path']
        alexnet.load_weights(weight_file)
        new_input = alexnet.input
        hidden_layers = alexnet.layers[-6].output
        
        dense_6 = Dense(6,name='dense_6')(hidden_layers)
        prediction = Activation('softmax', name='softmax')(dense_6)

        alexnet = Model(input=alexnet.input, output=prediction)

        # print alexnet.layers
        for layer in alexnet.layers[:-3]:
            layer.trainable = False
        
        # for layer in alexnet.layers:
            # print layer, layer.trainable
        
        alexnet.save_weights('./model_weights/final_alexnet1.h5') 
        learning_rate,decay,momentum,nesterov,optimizers,loss,metrics = None,None,None,None,None,None,None

        with open('./configurations/tl_configurations.json') as json_data_file:
            data = json.load(json_data_file)
            learning_rate = data['SGD']['lr']
            decay = data['SGD']['decay']
            momentum = data['SGD']['momentum']
            nesterov = data['SGD']['nesterov']
            loss = data['compiler_details']['loss']
            metrics = data['compiler_details']['metrics']

        stoch_grad_decent = SGD(lr=learning_rate,decay=decay,momentum=momentum,nesterov=nesterov)
        alexnet.compile(optimizer=stoch_grad_decent,loss=loss,metrics=metrics)
        alexnet.summary()
        return alexnet

    def fine_tune_tl_alexnet_images(self,image_category_dict):
        f_generator = FitGenerator()
        preprocess = Preprocess_Images()

        train_directory = None
        train_batch_size = None
        train_shuffle = None
        train_color_mode = None
        train_class_mode = None
        train_target_size = None
        train_seed = None

        validation_directory = None
        validation_batch_size = None
        validation_shuffle = None
        validation_color_mode = None
        validation_class_mode = None
        validation_target_size = None
        validation_seed = None

        train_shear_range = None
        train_zoom_range = None
        train_horizontal_flip = None

        validation_shear_range = None
        validaion_zoom_range = None
        validation_horizontal_flip = None

        num_classes = None
        epochs = None
        
        with open('./configurations/tl_configurations.json') as json_data_file:
            data = json.load(json_data_file)
            epochs = data['fine_tune']['epochs']
            num_classes = data['fine_tune']['num_classes']

            train_directory = data['train_generator']['directory']
            train_batch_size = data['train_generator']['batch_size']
            train_shuffle = data['train_generator']['shuffle']
            train_color_mode = data['train_generator']['color_mode']
            train_class_mode = data['train_generator']['class_mode']
            train_target_size = (data['train_generator']['target_size_width'],data['train_generator']['target_size_height'])
            train_seed = data['train_generator']['seed']

            validation_directory = data['validation_generator']['directory']
            validation_batch_size = data['validation_generator']['batch_size']
            validation_shuffle = data['validation_generator']['shuffle']
            validation_color_mode = data['validation_generator']['color_mode']
            validation_class_mode = data['validation_generator']['class_mode']
            validation_target_size = (data['validation_generator']['target_size_width'],data['validation_generator']['target_size_height'])
            validation_seed = data['validation_generator']['seed']

            train_shear_range =  data['train_data_generator']['shear_range']
            train_zoom_range =  data['train_data_generator']['zoom_range']
            train_horizontal_flip =  data['train_data_generator']['horizontal_flip']

            validation_shear_range =  data['validation_data_generator']['shear_range']
            validation_zoom_range =  data['validation_data_generator']['zoom_range']
            validation_horizontal_flip =  data['validation_data_generator']['horizontal_flip']
            
        # types = ['anger','disgust','fear','happiness','sadness','surprise']

        # category_array = []
        # image_array = []
        # for type_c in types:
        #     images = image_category_dict[type_c]
        #     for image in images:
        #         image_array.append(image)
        #         category_array.append(type_c)
        
        # image_array = np.array(image_array)
        # train_y = np.asarray(category_array)
        # label_encoder = LabelEncoder()
        # train_y = label_encoder.fit_transform(train_y)
        # train_y = to_categorical(train_y)
        # X_train, X_valid, Y_train, Y_valid = train_test_split(image_array,train_y,test_size=test_size, random_state=random_state)

        # self.transer_to_image_data_generator(X_train)

        model = self.get_tl_frozen_alexnet(num_classes)

        train_datagen = ImageDataGenerator(
            rescale = 1./255,
            shear_range=train_shear_range,
            zoom_range = train_zoom_range,
            horizontal_flip = train_horizontal_flip
        )

        valid_datagen = ImageDataGenerator(
            rescale = 1./255,
            shear_range= validation_shear_range,
            zoom_range = validation_zoom_range,
            horizontal_flip = validation_horizontal_flip
        )

        train_generator = train_datagen.flow_from_directory(
            directory=train_directory,
            target_size = train_target_size,
            color_mode = train_color_mode,
            batch_size = train_batch_size,
            class_mode = train_class_mode,
            shuffle =train_shuffle,
            seed = train_seed
        )

        validation_generator = valid_datagen.flow_from_directory(
            directory=validation_directory,
            target_size = validation_target_size,
            color_mode = validation_color_mode,
            batch_size = validation_batch_size,
            class_mode = validation_class_mode,
            shuffle =validation_shuffle,
            seed = validation_seed
        )

        # history = model.fit_generator(f_generator.generator(X_train, Y_train, batch_size), nb_epoch=10, samples_per_epoch=len(X_train)//batch_size)
        history = model.fit_generator(
            generator = train_generator,
            steps_per_epoch=train_generator.n//train_generator.batch_size,
            epochs=epochs,
            validation_data=validation_generator,
            validation_steps=validation_generator.n//validation_generator.batch_size
        )

        
        model.save_weights('./model_weights/final_alexnet.h5') 
        acc = history.history['acc']
        # val_acc = history.history['val_acc']
        loss = history.history['loss']
        # val_loss = history.history['val_loss']

        epochs = range(len(acc))
        plt.plot(epochs, acc, 'b', label='Traning Accuracy')
        plt.plot(epochs, val_acc, 'r', label='Validation Accuracy')
        plt.title('TrainingAccuracy')
        plt.legend()
        plt.tight_layout()
        # plt.show()
        plt.savefig('training_accuracy.jpg',bbox_inches='tight')
        # plt.show()
        plt.figure()
        plt.plot(epochs, loss, 'b', label='Traning Loss')
        plt.plot(epochs, val_loss, 'r', label='Validation Loss')
        plt.title('Training Loss')
        plt.legend()
        plt.tight_layout()
        # plt.show()
        plt.savefig('training_loss.jpg',bbox_inches='tight')
        # plt.show()


