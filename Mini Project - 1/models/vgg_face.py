from keras.models import Model
from keras.layers import Input, Convolution2D, ZeroPadding2D, MaxPooling2D, Flatten, Dropout, Activation
import numpy as np

class VGG_Face:

    def get_vgg_model(self):

        # image = Input(shape=(3,224,224))
        image = Input(shape=(224,224,3))
        padding_1_1 = ZeroPadding2D(padding=(1,1))(image)
        convolution_1_1 = Convolution2D(64,3,3,activation='relu',name='conv_1_1',dim_ordering='tf')(padding_1_1)
        padding_1_2 = ZeroPadding2D(padding=(1,1))(convolution_1_1)
        convolution_1_2 = Convolution2D(64,3,3,activation='relu',name='conv_1_2')(padding_1_2)
        pooling_1 = MaxPooling2D((2,2),strides=(2,2))(convolution_1_2)

        padding_2_1 = ZeroPadding2D(padding=(1,1))(pooling_1)
        convolution_2_1 = Convolution2D(128,3,3,activation='relu',name='conv_2_1')(padding_2_1)
        padding_2_2 = ZeroPadding2D(padding=(1,1))(convolution_2_1)
        convolution_2_2 = Convolution2D(128,3,3,activation='relu',name='conv_2_2')(padding_2_2)
        pooling_2 = MaxPooling2D((2,2),strides=(2,2))(convolution_2_2)

        padding_3_1 = ZeroPadding2D(padding=(1,1))(pooling_2)
        convolution_3_1 = Convolution2D(256,3,3,activation='relu',name='conv_3_1')(padding_3_1)
        padding_3_2 = ZeroPadding2D(padding=(1,1))(convolution_3_1)
        convolution_3_2 = Convolution2D(256,3,3,activation='relu',name='conv_3_2')(padding_3_2)
        padding_3_3 = ZeroPadding2D(padding=(1,1))(convolution_3_2)
        convolution_3_3 = Convolution2D(256,3,3,activation='relu',name='conv_3_3')(padding_3_3)
        pooling_3 = MaxPooling2D((2,2),strides=(2,2))(convolution_3_3)

        padding_4_1 = ZeroPadding2D(padding=(1,1))(pooling_3)
        convolution_4_1 = Convolution2D(512,3,3,activation='relu',name='conv_4_1')(padding_4_1)
        padding_4_2 = ZeroPadding2D(padding=(1,1))(convolution_4_1)
        convolution_4_2 = Convolution2D(512,3,3,activation='relu',name='conv_4_2')(padding_4_2)
        padding_4_3 = ZeroPadding2D(padding=(1,1))(convolution_4_2)
        convolution_4_3 = Convolution2D(512,3,3,activation='relu',name='conv_4_3')(padding_4_3)
        pooling_4 = MaxPooling2D((2,2),strides=(2,2))(convolution_4_3)

        padding_5_1 = ZeroPadding2D(padding=(1,1))(pooling_4)
        convolution_5_1 = Convolution2D(512,3,3,activation='relu',name='conv_5_1')(padding_5_1)
        padding_5_2 = ZeroPadding2D(padding=(1,1))(convolution_5_1)
        convolution_5_2 = Convolution2D(512,3,3,activation='relu',name='conv_5_2')(padding_5_2)
        padding_5_3 = ZeroPadding2D(padding=(1,1))(convolution_5_2)
        convolution_5_3 = Convolution2D(512,3,3,activation='relu',name='conv_5_3')(padding_5_3)
        pooling_5 = MaxPooling2D((2,2),strides=(2,2))(convolution_5_3)

        dense_6 = Convolution2D(4096,7,7,activation='relu',name='dense_6',dim_ordering='tf')(pooling_5)
        dense_6_dropout = Dropout(0.5)(dense_6)
        dense_7 = Convolution2D(4096,1,1,activation='relu',name='dense_7')(dense_6_dropout)
        dense_7_dropout = Dropout(0.5)(dense_7)
        dense_8 = Convolution2D(2622,1,1,name='dense_8')(dense_7_dropout)

        flat = Flatten()(dense_8)

        vgg_out = Activation('softmax')(flat)

        vgg_model = Model(input=image, output=vgg_out)

        return vgg_model




