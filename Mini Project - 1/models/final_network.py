import sys
sys.path.insert(0, "./")
import warnings
import json
import os
import cv2
import numpy as np
from models.transfer_learning.tl_vggnet import TransferLearnedFrozenVGGNet
from preprocess.preprocess_input_images import Preprocess_Images

from keras.utils import plot_model

if not sys.warnoptions:
    warnings.simplefilter("ignore")

def sampling_strategy(viode_path):
    cap = cv2.VideoCapture(viode_path)
    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    lists = np.array_split(range(length), 10)
    sampling_locations = []
    for item in lists:
        sampling_locations.append(item[-1])
    
    frames = []
    if cap.isOpened():
        count = 0
        success, image = cap.read()
        while success:
            if count in sampling_locations:
                frames.append(image)
            success, image = cap.read()
            count += 1
    return frames

def save_vgg_data(path):
    save_path = None
    with open('./configurations/vgg_configurations.json') as json_data_file:
            data = json.load(json_data_file)
            save_path = data['vgg_frame_extraction']['image_save_path']
    
    train_img=[]
    
    files_folders = os.listdir(path)
    folders = []
    for item in files_folders:
        if os.path.isdir(path+'/'+item):
            folders.append(path+'/'+item)
    
    sub_folders = ['anger','disgust','fear','happiness','sadness','surprise']

    
    # file_dict = {}
    # file_dict['anger']=[]
    # file_dict['disgust']=[]
    # file_dict['fear']=[]
    # file_dict['happiness']=[]
    # file_dict['sadness']=[]
    # file_dict['surprise']=[]

    for folder in folders:
        sub_folders = os.listdir(folder)
        for sub_folder in sub_folders:
            sentences = os.listdir(folder+'/'+sub_folder)
            # print files
            
            for sentence in sentences:
                # print 'executed'
                video_file = [f for f in os.listdir(folder+'/'+sub_folder+'/'+sentence) if f.endswith('.avi')][0]
                # print folder+'/'+sub_folder+'/'+sentence+'/'+video_file
                # print '=='
                vidcap = cv2.VideoCapture(folder+'/'+sub_folder+'/'+sentence+'/'+video_file)

                length = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
                lists = np.array_split(range(length), 10)
                sampling_locations = []
                for item in lists:
                    sampling_locations.append(item[-1])
                
                image_array = []
                if vidcap.isOpened():
                    count = 0
                    success, image = vidcap.read()
                    while success:
                        if count in sampling_locations:
                            image = cv2.resize(image, (224, 224))
                            image_array.append(image)
                        success, image = vidcap.read()
                        count += 1

                
                for i in range(len(image_array)):
                # for i in range((len(segmented_inputs)-3)/3):
                    # file_dict[sub_folder].append(segmented_inputs[i])
                    # mfcc_extraction.save_as_image(segmented_inputs[i],sub_folder,video_file[:-4],save_path,i)
                    image_path = save_path+'/'+sub_folder+'/'+video_file.split('.')[0]+"_"+str(i)+".jpg"
                    print image_path
                    cv2.imwrite(image_path,image_array[i])
                    # mfcc_extraction.save_as_image_segments(segmented_inputs[i*3],sub_folder,video_file[:-4],save_path,i,"static")
                    # mfcc_extraction.save_as_image_segments(segmented_inputs[i*3+1],sub_folder,video_file[:-4],save_path,i,"delta")
                    # mfcc_extraction.save_as_image_segments(segmented_inputs[i*3+2],sub_folder,video_file[:-4],save_path,i,"delta_delta")
                # print video_file, sub_folder


weight_file = None
image_directory = None

with open('./configurations/tl_configurations_vgg.json') as json_data_file:
	data = json.load(json_data_file)
	weight_file = data['tl_data']['weights_path']
	image_directory = data['tl_data']['images_dir_path']


images_dict = {}
images_dict['anger'] = []
images_dict['disgust'] = []
images_dict['happiness'] = []
images_dict['sadness'] = []
images_dict['fear'] = []
images_dict['surprise'] = []

images_list_unicode = os.listdir(image_directory)
for folder in images_list_unicode:
	images_list = os.listdir(image_directory+folder+'/')
	image_batch = []
	for image in images_list:
		image_batch.append(image_directory+folder+'/'+image)
	# processed_image_batch = preprocess.preprocess_images_alexnet(image_batch)
	# images_dict[folder] = processed_image_batch

	images_dict[folder] = image_batch

base_model = TransferLearnedFrozenVGGNet().fine_tune_tl_vgg_face_images(images_dict)


# save_vgg_data('/home/darshana/enterface database')

# frames_list = sampling_strategy("./test/test_videos/s1_fe_2.avi")

# base_model = TransferLearnedFrozenVGGNet().get_tl_frozen_vggnet(6)

# feat_extractor = Model(input=base_model.input,output=base_model.get_layer("dense_6").output)


# x=TimeDistributed(feat_extractor)(your_input)
# x=LSTM(300,return_sequences=False)(x)
# output=Dense(6, activation='softmax')(x)

# model=Model(input=your_input,output=output )
