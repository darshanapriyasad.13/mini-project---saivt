import sys
sys.path.insert(0, "./")

import json
import numpy as np
from keras.layers import Activation
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import merge
from keras.layers.convolutional import Convolution2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.convolutional import ZeroPadding2D
from keras.models import Model
from keras.models import Sequential
from keras.optimizers import SGD
from convnetskeras.customlayers import crosschannelnormalization
from convnetskeras.customlayers import splittensor

# mallem song
class AlexNet:

    def get_alexnet_model(self):
        
        inputs = Input(shape=(3, 227, 227))

        # Convolutional Layer 1
        conv_1 = Convolution2D(96, 11, 11, subsample=(4, 4), activation='relu',name='conv_1',dim_ordering='th')(inputs)

        # Convolutional Layer 2
        conv_2 = MaxPooling2D(pool_size=(3, 3), strides=(2, 2),dim_ordering='th')(conv_1)
        conv_2 = crosschannelnormalization(name='convpool_1')(conv_2)
        conv_2 = ZeroPadding2D((2, 2))(conv_2)
        conv_2 = merge([
                        Convolution2D(128, 5, 5, activation='relu', name='conv_2_' + str(i + 1))(
                            splittensor(ratio_split=2, id_split=i)(conv_2)
                        ) for i in range(2)], mode='concat', concat_axis=1, name='conv_2')

        # Convolutional Layer 3
        conv_3 = MaxPooling2D((3, 3), strides=(2, 2))(conv_2)
        conv_3 = crosschannelnormalization()(conv_3)
        conv_3 = ZeroPadding2D((1, 1))(conv_3)
        conv_3 = Convolution2D(384, 3, 3, activation='relu', name='conv_3')(conv_3)
        
        # Convolutional Layer 4
        conv_4 = ZeroPadding2D((1, 1))(conv_3)
        conv_4 = merge([
                        Convolution2D(192, 3, 3, activation='relu', name='conv_4_' + str(i + 1))(
                            splittensor(ratio_split=2, id_split=i)(conv_4)
                        ) for i in range(2)], mode='concat', concat_axis=1, name='conv_4')

        # Convolutional Layer 5
        conv_5 = ZeroPadding2D((1, 1))(conv_4)
        conv_5 = merge([
                        Convolution2D(128, 3, 3, activation='relu', name='conv_5_' + str(i + 1))(
                            splittensor(ratio_split=2, id_split=i)(conv_5)
                        ) for i in range(2)], mode='concat', concat_axis=1, name='conv_5')

        # Dense Layer 1
        dense_1 = MaxPooling2D((3, 3), strides=(2, 2), name='convpool_5')(conv_5)
        dense_1 = Flatten(name='flatten')(dense_1)
        dense_1 = Dense(4096, activation='relu', name='dense_1')(dense_1)

        # Dense Layer 2
        dense_2 = Dropout(0.5)(dense_1)
        dense_2 = Dense(4096, activation='relu', name='dense_2')(dense_2)

        # Dense Layer 3
        dense_3 = Dropout(0.5)(dense_2)
        dense_3 = Dense(1000, name='dense_3')(dense_3)

        # Prediction Layer
        prediction = Activation('softmax', name='softmax')(dense_3)

        model = Model(input=inputs, output=prediction)

        loss_type, optimizer_type, metric_types = None, None, None
        data = None
        with open('./configurations/alexnet_configurations.json') as json_data_file:
            data = json.load(json_data_file)
            loss_type = data['alexnet']['loss']
            optimizer_type = data['alexnet']['optimizer']
            metric_types = data['alexnet']['metrics']

        model.compile(loss=loss_type, optimizer=optimizer_type,metrics=metric_types)
        
        return model