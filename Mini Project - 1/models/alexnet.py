import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Flatten, Conv2D, MaxPooling2D, ZeroPadding2D
from keras.layers.normalization import BatchNormalization
import numpy as np

class AlexNet:

    def get_alexnet_model(self):
        
        np.random.seed(1000)
        model = Sequential()

        # 1st Convolution Layer
        model.add(Conv2D(filters=96, input_shape=(227,227,3), kernel_size=(11,11),strides=(4,4)))
        # model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2), padding='valid'))

        # 2nd Convolutional Layer
        model.add(ZeroPadding2D(2))
        model.add(Conv2D(filters=256, kernel_size=(5,5), strides=(1,1), padding='valid'))
        # model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2), padding='valid'))

        # 3rd Convolutional Layer
        model.add(ZeroPadding2D(1))
        model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
        model.add(Activation('relu'))

        # 4th Convolutional Layer
        model.add(ZeroPadding2D(1))
        model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
        model.add(Activation('relu'))

        # 5th Convolutional Layer
        model.add(ZeroPadding2D(1))
        model.add(Conv2D(filters=256, kernel_size=(3,3), strides=(1,1), padding='valid'))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2), padding='valid'))

        # Passing it to a dense layer
        model.add(Flatten())

        # 1st Dense Layer
        model.add(Dense(4096))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))

        # 2nd Dense Layer
        model.add(Dense(4096))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))

        # Output Layer
        model.add(Dense(1000))
        model.add(Activation('softmax'))

        model.summary()
        model.compile(loss='categorical_crossentropy', optimizer='adam',metrics=['accuracy'])
        return model
