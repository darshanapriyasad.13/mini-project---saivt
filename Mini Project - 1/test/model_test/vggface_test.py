import sys
sys.path.insert(0, "./")

import os
import json
import warnings
import numpy as np
from models.vgg_face import VGG_Face
from keras.utils import plot_model
from scipy.misc import imread
from scipy.misc import imresize
from keras.utils import plot_model

if not sys.warnoptions:
    warnings.simplefilter("ignore")

weight_file = None
image_path = None

with open('./configurations/vgg_configurations.json') as json_data_file:
	data = json.load(json_data_file)
	weight_file = data['vgg_face_test']['weights_path']
	image_path = data['vgg_face_test']['images_dir_path']

vgg_face = VGG_Face().get_vgg_model()
vgg_face.load_weights(weight_file)

image_list= []

image = imread(image_path, mode = 'RGB')

image = imresize(image,(224,224))

image = image.astype('float32')

	#    im[:,:,0] -= 129.1863
	#    im[:,:,1] -= 104.7624
	#    im[:,:,2] -= 93.5940

image = image.transpose((0,1,2))
image_list.append(image)

image_batch = np.stack(image_list,axis=0)
out = vgg_face.predict(image_batch)
plot_model(vgg_face, to_file='./summary/models/vgg_face.jpg')
print(out[0][0])

