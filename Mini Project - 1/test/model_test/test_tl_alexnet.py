import sys
sys.path.insert(0, "./")
import warnings
import json
import os
from models.transfer_learning.tl_alexnet import TransferLearnedFrozenAlexNet
from preprocess.preprocess_input_images import Preprocess_Images

from keras.utils import plot_model

if not sys.warnoptions:
    warnings.simplefilter("ignore")


# model = TransferLearnedFrozenAlexNet().get_tl_frozen_alexnet(6)

# plot_model(model, to_file='./summary/models/transfer_learned_modified_alexnet_model.jpg',show_shapes=True,show_layer_names=True)

weight_file = None
image_directory = None

with open('./configurations/tl_configurations.json') as json_data_file:
	data = json.load(json_data_file)
	weight_file = data['tl_data']['weights_path']
	image_directory = data['tl_data']['images_dir_path']

preprocess = Preprocess_Images()

images_dict = {}
images_dict['anger'] = []
images_dict['disgust'] = []
images_dict['happiness'] = []
images_dict['sadness'] = []
images_dict['fear'] = []
images_dict['surprise'] = []

images_list_unicode = os.listdir(image_directory)
for folder in images_list_unicode:
	images_list = os.listdir(image_directory+folder+'/')
	image_batch = []
	for image in images_list:
		image_batch.append(image_directory+folder+'/'+image)
	# processed_image_batch = preprocess.preprocess_images_alexnet(image_batch)
	# images_dict[folder] = processed_image_batch

	images_dict[folder] = image_batch


model = TransferLearnedFrozenAlexNet().fine_tune_tl_alexnet_images(images_dict)
