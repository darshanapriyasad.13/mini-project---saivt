import h5py
import numpy as np

def h5_layers(file_path):
    data = h5py.File(file_path, 'r')
    for key in data.keys():
        group = data[key]
        for sub_key in group.keys():
            print(sub_key),
            temp_data = group[sub_key].value
            print temp_data.shape,
        if(len(group.keys())>0):
            print '\n'


def npy_layers(file_path,layers):
    weights_dict = np.load(file_path, encoding='bytes').item()
    for layer in layers:
        layer_weights = weights_dict[layer][0]
        # print layer_weights
        layer_bias = weights_dict[layer][1]
        # print layer_bias
        print layer,layer_weights.shape,layer_bias.shape
        
def h5_from_npy(file_path_npy,file_path_h5,layers):
    weights_dict = np.load(file_path_npy, encoding='bytes').item()
    h5_file = h5py.File(file_path_h5, 'w')

    for layer in layers:
        # print "done "+layer
        group = h5_file.create_group(layer)
        layer_weights = weights_dict[layer][0]
        group.create_dataset(layer+'_w', data=layer_weights, dtype="f")
        layer_bias = weights_dict[layer][1]
        group.create_dataset(layer+'_b', data=layer_bias, dtype="f")

    h5_file.close()

# './test/alexnet_weights.h5'
# './test/bvlc_alexnet.npy'
layers = ['conv1','conv2','conv3','conv4','conv5','fc6','fc7','fc8']
npy_layers('./test/bvlc_alexnet.npy',layers)

# h5_from_npy('./test/bvlc_alexnet.npy','./test/bvlc_alexnet.h5',layers)