import sys
sys.path.insert(0, "./")
import json
import numpy as np
from audio_processing.mfcc_feature_extraction import MFCC_Extraction


audio_file = None

with open('./configurations/mfcc_configurations.json') as json_data_file:
    data = json.load(json_data_file)
    audio_file = data['mfcc_test']['video_path']


segmented_inputs = MFCC_Extraction().generate_dcnn_input(audio_file)