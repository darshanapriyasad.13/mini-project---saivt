import sys
sys.path.insert(0, "./")

import os
import json
import warnings
import numpy as np
from models.transfer_learning.tl_alexnet import TransferLearnedFrozenAlexNet
from preprocess.preprocess_input_images import Preprocess_Images
from keras.applications.imagenet_utils import decode_predictions
# from models.alexnet import AlexNet
from keras.utils import plot_model

if not sys.warnoptions:
    warnings.simplefilter("ignore")

weight_file = None
image_directory = None

with open('./configurations/tl_configurations.json') as json_data_file:
	data = json.load(json_data_file)
	weight_file = data['tl_test']['weights_path']
	image_directory = data['tl_test']['images_dir_path']


alexnet = TransferLearnedFrozenAlexNet().get_tl_frozen_alexnet(6)
alexnet.load_weights(weight_file)

# plot_model(alexnet, to_file='./summary/models/general_alexnet_model.jpg',show_shapes=True,show_layer_names=True)

preprocess = Preprocess_Images()

images_list_unicode = os.listdir(image_directory)
images_list = []

for image in images_list_unicode:
	images_list.append(image_directory.encode("utf-8")+image.encode("utf-8"))
# images_list = [(os.path.abspath(x)).encode("utf-8") for x in os.listdir(image_directory)]
# print images_list
processed_image_batch = preprocess.preprocess_images_alexnet(images_list)
# print processed_image_batch
prediction_output = alexnet.predict(processed_image_batch,verbose = 1)

print prediction_output
b=np.zeros_like(prediction_output)
b[np.arange(len(prediction_output)),prediction_output.argmax(1)] = 1

for i in range(len(images_list)):
    print images_list[i],b[i]
# print b
# print np.argmax(prediction_output)
# decoded_output = decode_predictions(prediction_output)

# for j in range(len(images_list)):
# 	print images_list_unicode[j].encode('utf-8')
# 	for (i, (imagenetID, label, prob)) in enumerate(decoded_output[j]):
# 		print("{}. {}: {:.2f}%".format(i + 1, label, prob * 100))

